---
date: 1 Jun 2022
author: Toby C. Wilkinson
---

# Fieldwalker and SoMS Data with JARE Scripts

`https://gitlab.com/fieldwalker/fieldwalker-soms`

Repository for data from the _Survey of Mediterranean Surveys (SoMS)_ and _Fieldwalker.org New Project_ contributions, with scripts in R used to generate publication figures for JARE article (by Alex Knodell, Toby Wilkinson, Thomas Leppard, Hector Orengo).

## Survey of Mediterranean Surveys (SoMS) data

The data from the raw contributions to the SoMS (Survey of Mediterranean Surveys)
and the Fieldwalker.org "new project" questionnaire are included, under `/sources/`.

In both cases _excluding_ any private data such as email addresses, but _including_ any
surveys submitted which did not fall within the Mediterranean region (and hence not
included in the analysis for the JARE article).

We are grateful for those who submitted data to these surveys, who are
acknowledged in the `/sources/acknowledgements.md` file.

## R scripts for JARE article figures and graphs

Scripts were made using R version 3.4 with various packages. All scripts article
contained under `/scripts/` . The most important of these is the
`jar_comparative_plots_rev2.R` script, which loads all the relevant
package libraries, and runs `jar_clean_soms.R` to load data before forming the
relevant plots and saving to `/output/`.

Note: NOT included in this repository is the data loaded to plot the biome/ecoregions from
the TNC ecoregions dataset, more information about which can be accessed from here:
http://maps.tnc.org/files/metadata/TerrEcos.xml

Please also cite the following article in reference to these figures and code:

<cite>
A. Knodell, T. Wilkinson, T. Leppard, H. Orengo (2022). Survey Archaeology in the Mediterranean World: Regional Traditions and Contributions to Long-Term History. _Journal of Archaeological Research_ __xx__: pp. xx-xx. doi: [10.xxxxxx](https://dx.doi.org/10.xxxxxx)
</cite>
